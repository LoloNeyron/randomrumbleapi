<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //users
        DB::table('users')->insert([
            'name' => 'Loic Neyron',
            'email' => 'loicneyron@gmail.com',
            'password' => '$2y$10$r/psb5G4lHjPvD79/Od0tOT8wW0rQwKxe6b/xP9ejqqNJccK5S2za',
            'api_token' => 'MkwItoxGuOtLsiDsgqstyz4wnIHqdiBrs4DbFnfk9L4p4imn1F4R9XhxznjDUww2qAwWxCAva5THQ3kh',
        ]);

        DB::table('users')->insert([
            'name' => 'Alexandre Loretzin',
            'email' => 'a.loretzin@gmail.com',
            'password' => '$2y$10$K5ydKkdPdNaCeBas/mi.rezuAIiP0OC5ORbgTYf1UmIgqXLGjjygu',
            'api_token' => '1EfNOoxhkUMVEpgcKn9IJmhAU6BwyUVsjdfAWJv4Oub5rjCF0TjSTgsJFayYDV0Ugjrhbpv7reas9ncb',
        ]);

        //Character names
        DB::table('character_names')->insert([
            'name'=> 'Anderson',
        ]);
        DB::table('character_names')->insert([
            'name'=> 'Ashford',
        ]);
        DB::table('character_names')->insert([
            'name'=> 'Bacall',
        ]);
        DB::table('character_names')->insert([
            'name'=> 'Baker',
        ]);
        DB::table('character_names')->insert([
            'name'=> 'Barnett',
        ]);
        DB::table('character_names')->insert([
            'name'=> 'Kennedy',
        ]);
        DB::table('character_names')->insert([
            'name'=> 'Nelson',
        ]);
        DB::table('character_names')->insert([
            'name'=> 'Redford',
        ]);
        DB::table('character_names')->insert([
            'name'=> 'Sands',
        ]);
        DB::table('character_names')->insert([
            'name'=> 'Tyler',
        ]);

        $numberOfCharacterNames = \App\CharacterName::count();

        //capacities
        DB::table('capacities')->insert([
            'type' => 'Damages',
            'name' => 'coup de poing',
            'description' => 'met un coup de poing',
            'dmg' => '4',
            'heal' => '0',
            'mana' => '0',
            'buff' => '2',
            'gainMana' => '0',
            'combo' => '3',
        ]);
        DB::table('capacities')->insert([
            'type' => 'Damages',
            'name' => 'coup de pied',
            'description' => 'met un coup de poing',
            'dmg' => '6',
            'heal' => '0',
            'mana' => '2',
            'buff' => '2',
            'gainMana' => '0',
            'combo' => '3',
        ]);
        DB::table('capacities')->insert([
            'type' => 'heal',
            'name' => 'heal',
            'description' => 'met un coup de poing',
            'dmg' => '0',
            'heal' => '5',
            'mana' => '3',
            'buff' => '2',
            'gainMana' => '0',
            'combo' => '3',
        ]);
        DB::table('capacities')->insert([
            'type' => 'mana',
            'name' => 'mana',
            'description' => 'met un coup de poing',
            'dmg' => '0',
            'heal' => '2',
            'mana' => '1',
            'buff' => '2',
            'gainMana' => '6',
            'combo' => '3',
        ]);

        $numberOfCapacities = \App\Capacities::count();

        //personnages
        $cap1 = rand(1, $numberOfCapacities);
        $cap2 = rand(1, $numberOfCapacities);
        $cap3 = rand(1, $numberOfCapacities);
        $cap4 = rand(1, $numberOfCapacities);
        DB::table('character')->insert([
            'id_user'=> 1,
            'id_name' => rand(1, $numberOfCharacterNames),
            'ids_capacities' => "{$cap1},{$cap2},{$cap3},{$cap4}",
            'pv' => rand(0, 150),
            'pvMax' => rand(0, 150),
            'mana' => rand(0, 150),
            'manaMax' => rand(0, 150),
            'hasPlayed' => false
        ]);
        $cap1 = rand(1, $numberOfCapacities);
        $cap2 = rand(1, $numberOfCapacities);
        $cap3 = rand(1, $numberOfCapacities);
        $cap4 = rand(1, $numberOfCapacities);
        DB::table('character')->insert([
            'id_user'=> 1,
            'id_name' => rand(1, $numberOfCharacterNames),
            'ids_capacities' => "{$cap1},{$cap2},{$cap3},{$cap4}",
            'pv' => rand(0, 150),
            'pvMax' => rand(0, 150),
            'mana' => rand(0, 150),
            'manaMax' => rand(0, 150),
            'hasPlayed' => false
        ]);
        $cap1 = rand(1, $numberOfCapacities);
        $cap2 = rand(1, $numberOfCapacities);
        $cap3 = rand(1, $numberOfCapacities);
        $cap4 = rand(1, $numberOfCapacities);
        DB::table('character')->insert([
            'id_user'=> 1,
            'id_name' => rand(1, $numberOfCharacterNames),
            'ids_capacities' => "{$cap1},{$cap2},{$cap3},{$cap4}",
            'pv' => rand(0, 150),
            'pvMax' => rand(0, 150),
            'mana' => rand(0, 150),
            'manaMax' => rand(0, 150),
            'hasPlayed' => false
        ]);
        $cap1 = rand(1, $numberOfCapacities);
        $cap2 = rand(1, $numberOfCapacities);
        $cap3 = rand(1, $numberOfCapacities);
        $cap4 = rand(1, $numberOfCapacities);
        DB::table('character')->insert([
            'id_user'=> 1,
            'id_name' => rand(1, $numberOfCharacterNames),
            'ids_capacities' => "{$cap1},{$cap2},{$cap3},{$cap4}",
            'pv' => rand(10, 150),
            'pvMax' => rand(10, 150),
            'mana' => rand(10, 150),
            'manaMax' => rand(10, 150),
            'hasPlayed' => false
        ]);

        //Monster
        DB::table('monster')->insert([
            'pv' => rand(100, 800),
            'pvMax' => rand(100, 800),
            'mana' => rand(100, 800),
            'manaMax' => rand(100, 800),
            'attack' => rand(10, 20)
        ]);
    }
}
