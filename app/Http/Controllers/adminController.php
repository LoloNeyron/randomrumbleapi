<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class adminController extends Controller
{
    public function index() {
        return view('admin.index');
    }

    public function generateTheFirstState(){

    }

    public function gestionGames() {
        $gameData = DB::table('game')->get();
        return view('admin.game', ["gameData" => $gameData]);
    }

    public function gestionStates() {
        $stateData = DB::table('state')->get();
        return view('admin.state', ["stateData" => $stateData]);
    }

    public function gestionMonsters () {

    }

    public function gestionCapacities () {
        $capacitiesData = DB::table('capacities')->get();
        return view('admin.capacitites', ["capacitiesData" => $capacitiesData]);
    }
}
