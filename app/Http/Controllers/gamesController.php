<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Game;
use App\User;

class gamesController extends Controller
{
    public function getGameById($id = null) {
        if($id === null){
            return DB::table('game')->get();
        }else{
            return DB::table('game')->where('id', $id)->get();
        }
    }

    public function getGameByUserId($user_id) {
        return DB::table('game')->where('user_id', $user_id)->get();
    }

    public function createGame(Request $request) {

        $user_id = $request->input('id_user');
        $api_token = $request->input('api_token');
        $user = User::find($user_id);

        if($user->api_token === $api_token){
            $game = new Game();

            $game->user_id = 1;
            $game->state = 'test';
            $game->save();

            return true;
        }else{
            return 'You don\'t have the access to use this /*Games Controller L.29*/';
        }
    }
}
