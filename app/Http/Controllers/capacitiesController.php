<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Capacities;
use App\User;

class capacitiesController extends Controller
{
    public function createCapacitie(Request $request) {

        $user_id = $request->input('user_id');
        $api_token = $request->input('api_token');
        $user = User::find($user_id);

        if($user->api_token === $api_token){
            $cap = new Capacities();

            $cap->type = $request->input('nc_type');
            $cap->name = $request->input('nc_name');
            $cap->description = $request->input('nc_desc');
            $cap->heal = $request->input('nc_heal');
            $cap->dmg = $request->input('nc_dmg');
            $cap->mana = $request->input('nc_mana');
            $cap->gainMana = $request->input('nc_gainMana');
            $cap->buff = $request->input('nc_buff');
            $cap->combo = $request->input('nc_cmb');
            $cap->save();

            return back();
        }else{
            return 'You don\'t have the access to use this /*Games Controller L.29*/';
        }
    }

    public function destroyCapacities(Request $request) {
        $user_id = $request->input('user_id');
        $api_token = $request->input('api_token');
        $user = User::find($user_id);

        if($user->api_token === $api_token){
            Capacities::destroy($request->input('capacities_id'));
            return back();
        }
    }

    public function duplicateCapacities(Request $request) {
        $user_id = $request->input('user_id');
        $api_token = $request->input('api_token');
        $user = User::find($user_id);

        if($user->api_token === $api_token){
            $lastCapacities = Capacities::find($request->input('capacities_id'));

            $newCap = $lastCapacities->replicate();
            $newCap->save();

            return back();
        }
    }
}
