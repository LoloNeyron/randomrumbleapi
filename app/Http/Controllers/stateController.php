<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Game;
use App\State;
use App\User;

class stateController extends Controller
{
    public function getState($id = null) {
        if($id === null){
            return DB::table('state')->get();
        }else{
            return DB::table('state')->where('id', $id)->get();
        }
    }

    public function getStateByUserId($user_id) {
        return DB::table('state')->where('user_id', $user_id)->get();
    }

    public function getStateByGameId($game_id) {
        return DB::table('state')->where('game_id', $game_id)->get();
    }

    public function createState(Request $request) {

        $user_id = $request->input('id_user');
        $game_id = $request->input('id_game');
        $api_token = $request->input('api_token');
        $user = User::find($user_id);

        if($user->api_token === $api_token){
            $state = new State();

            $state->user_id = $user_id;
            $state->game_id = $game_id;
            $state->state = $request->input('state');
            $state->save();

            return true;
        }else{
            return 'You don\'t have the access to use this /*state Controller L.32*/';
        }
    }
}
