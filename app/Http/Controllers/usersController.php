<?php

namespace App\Http\Controllers;

use App\Character;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class usersController extends Controller
{
    public function getInitialState(){

        $numberOfCharacter = \App\Character::count();
        $numberOfMonster = \App\Monster::count();

        $character = DB::table('character')->where('id', rand(1, $numberOfCharacter))->get();
        $character = $character[0];
        $monster = DB::table('monster')->where('id', rand(1, $numberOfMonster))->get();
        $monster = $monster[0];
        $characterName = DB::table('character_names')->where('id', $character->id_name)->get('name');

        // $character->id = $character->id + 1;
        $ids_capacities = $character->ids_capacities;
        $arrOfIdsCapacities = explode(',', $ids_capacities);
        $capacities = [];

        foreach ($arrOfIdsCapacities as $cap){
            array_push($capacities, DB::table('capacities')->where('id', $cap)->get());
        }

        

        $output = '{
            "players": {
                "1": {
                    "name": "'.$characterName[0]->name.'",
                    "pv": '.$character->pv.',
                    "pvMax": '.$character->pvMax.',
                    "mana": '.$character->mana.',
                    "manaMax": '.$character->manaMax.',
                    "id": 1,
                    "hasPlayed": false,
                    "capacities": [
                        {
                            "type": "'.$capacities[0][0]->type.'",
                            "name": "'.$capacities[0][0]->name.'",
                            "description": "'.$capacities[0][0]->description.'",
                            "heal": '.$capacities[0][0]->heal.',
                            "dmg": '.$capacities[0][0]->dmg.',
                            "mana": '.$capacities[0][0]->mana.',
                            "gainMana": '.$capacities[0][0]->gainMana.',
                            "buff": '.$capacities[0][0]->buff.',
                            "combo": '.$capacities[0][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[1][0]->type.'",
                            "name": "'.$capacities[1][0]->name.'",
                            "description": "'.$capacities[1][0]->description.'",
                            "heal": '.$capacities[1][0]->heal.',
                            "dmg": '.$capacities[1][0]->dmg.',
                            "mana": '.$capacities[1][0]->mana.',
                            "gainMana": '.$capacities[1][0]->gainMana.',
                            "buff": '.$capacities[1][0]->buff.',
                            "combo": '.$capacities[1][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[2][0]->type.'",
                            "name": "'.$capacities[2][0]->name.'",
                            "description": "'.$capacities[2][0]->description.'",
                            "heal": '.$capacities[2][0]->heal.',
                            "dmg": '.$capacities[2][0]->dmg.',
                            "mana": '.$capacities[2][0]->mana.',
                            "gainMana": '.$capacities[2][0]->gainMana.',
                            "buff": '.$capacities[2][0]->buff.',
                            "combo": '.$capacities[2][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[3][0]->type.'",
                            "name": "'.$capacities[3][0]->name.'",
                            "description": "'.$capacities[3][0]->description.'",
                            "heal": '.$capacities[3][0]->heal.',
                            "dmg": '.$capacities[3][0]->dmg.',
                            "mana": '.$capacities[3][0]->mana.',
                            "gainMana": '.$capacities[3][0]->gainMana.',
                            "buff": '.$capacities[3][0]->buff.',
                            "combo": '.$capacities[3][0]->combo.'
                        }
                    ]
                },
                "2": {
                    "name": "'.$characterName[0]->name.'",
                    "pv": '.$character->pv.',
                    "pvMax": '.$character->pvMax.',
                    "mana": '.$character->mana.',
                    "manaMax": '.$character->manaMax.',
                    "id": 2,
                    "hasPlayed": false,
                    "capacities": [
                        {
                            "type": "'.$capacities[0][0]->type.'",
                            "name": "'.$capacities[0][0]->name.'",
                            "description": "'.$capacities[0][0]->description.'",
                            "heal": '.$capacities[0][0]->heal.',
                            "dmg": '.$capacities[0][0]->dmg.',
                            "mana": '.$capacities[0][0]->mana.',
                            "gainMana": '.$capacities[0][0]->gainMana.',
                            "buff": '.$capacities[0][0]->buff.',
                            "combo": '.$capacities[0][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[1][0]->type.'",
                            "name": "'.$capacities[1][0]->name.'",
                            "description": "'.$capacities[1][0]->description.'",
                            "heal": '.$capacities[1][0]->heal.',
                            "dmg": '.$capacities[1][0]->dmg.',
                            "mana": '.$capacities[1][0]->mana.',
                            "gainMana": '.$capacities[1][0]->gainMana.',
                            "buff": '.$capacities[1][0]->buff.',
                            "combo": '.$capacities[1][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[2][0]->type.'",
                            "name": "'.$capacities[2][0]->name.'",
                            "description": "'.$capacities[2][0]->description.'",
                            "heal": '.$capacities[2][0]->heal.',
                            "dmg": '.$capacities[2][0]->dmg.',
                            "mana": '.$capacities[2][0]->mana.',
                            "gainMana": '.$capacities[2][0]->gainMana.',
                            "buff": '.$capacities[2][0]->buff.',
                            "combo": '.$capacities[2][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[3][0]->type.'",
                            "name": "'.$capacities[3][0]->name.'",
                            "description": "'.$capacities[3][0]->description.'",
                            "heal": '.$capacities[3][0]->heal.',
                            "dmg": '.$capacities[3][0]->dmg.',
                            "mana": '.$capacities[3][0]->mana.',
                            "gainMana": '.$capacities[3][0]->gainMana.',
                            "buff": '.$capacities[3][0]->buff.',
                            "combo": '.$capacities[3][0]->combo.'
                        }
                    ]
                },
                "3": {
                    "name": "'.$characterName[0]->name.'",
                    "pv": '.$character->pv.',
                    "pvMax": '.$character->pvMax.',
                    "mana": '.$character->mana.',
                    "manaMax": '.$character->manaMax.',
                    "id": 3,
                    "hasPlayed": false,
                    "capacities": [
                        {
                            "type": "'.$capacities[0][0]->type.'",
                            "name": "'.$capacities[0][0]->name.'",
                            "description": "'.$capacities[0][0]->description.'",
                            "heal": '.$capacities[0][0]->heal.',
                            "dmg": '.$capacities[0][0]->dmg.',
                            "mana": '.$capacities[0][0]->mana.',
                            "gainMana": '.$capacities[0][0]->gainMana.',
                            "buff": '.$capacities[0][0]->buff.',
                            "combo": '.$capacities[0][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[1][0]->type.'",
                            "name": "'.$capacities[1][0]->name.'",
                            "description": "'.$capacities[1][0]->description.'",
                            "heal": '.$capacities[1][0]->heal.',
                            "dmg": '.$capacities[1][0]->dmg.',
                            "mana": '.$capacities[1][0]->mana.',
                            "gainMana": '.$capacities[1][0]->gainMana.',
                            "buff": '.$capacities[1][0]->buff.',
                            "combo": '.$capacities[1][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[2][0]->type.'",
                            "name": "'.$capacities[2][0]->name.'",
                            "description": "'.$capacities[2][0]->description.'",
                            "heal": '.$capacities[2][0]->heal.',
                            "dmg": '.$capacities[2][0]->dmg.',
                            "mana": '.$capacities[2][0]->mana.',
                            "gainMana": '.$capacities[2][0]->gainMana.',
                            "buff": '.$capacities[2][0]->buff.',
                            "combo": '.$capacities[2][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[3][0]->type.'",
                            "name": "'.$capacities[3][0]->name.'",
                            "description": "'.$capacities[3][0]->description.'",
                            "heal": '.$capacities[3][0]->heal.',
                            "dmg": '.$capacities[3][0]->dmg.',
                            "mana": '.$capacities[3][0]->mana.',
                            "gainMana": '.$capacities[3][0]->gainMana.',
                            "buff": '.$capacities[3][0]->buff.',
                            "combo": '.$capacities[3][0]->combo.'
                        }
                    ]
                },
                "4": {
                    "name": "'.$characterName[0]->name.'",
                    "pv": '.$character->pv.',
                    "pvMax": '.$character->pvMax.',
                    "mana": '.$character->mana.',
                    "manaMax": '.$character->manaMax.',
                    "id": 4,
                    "hasPlayed": false,
                    "capacities": [
                        {
                            "type": "'.$capacities[0][0]->type.'",
                            "name": "'.$capacities[0][0]->name.'",
                            "description": "'.$capacities[0][0]->description.'",
                            "heal": '.$capacities[0][0]->heal.',
                            "dmg": '.$capacities[0][0]->dmg.',
                            "mana": '.$capacities[0][0]->mana.',
                            "gainMana": '.$capacities[0][0]->gainMana.',
                            "buff": '.$capacities[0][0]->buff.',
                            "combo": '.$capacities[0][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[1][0]->type.'",
                            "name": "'.$capacities[1][0]->name.'",
                            "description": "'.$capacities[1][0]->description.'",
                            "heal": '.$capacities[1][0]->heal.',
                            "dmg": '.$capacities[1][0]->dmg.',
                            "mana": '.$capacities[1][0]->mana.',
                            "gainMana": '.$capacities[1][0]->gainMana.',
                            "buff": '.$capacities[1][0]->buff.',
                            "combo": '.$capacities[1][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[2][0]->type.'",
                            "name": "'.$capacities[2][0]->name.'",
                            "description": "'.$capacities[2][0]->description.'",
                            "heal": '.$capacities[2][0]->heal.',
                            "dmg": '.$capacities[2][0]->dmg.',
                            "mana": '.$capacities[2][0]->mana.',
                            "gainMana": '.$capacities[2][0]->gainMana.',
                            "buff": '.$capacities[2][0]->buff.',
                            "combo": '.$capacities[2][0]->combo.'
                        },
                        {
                            "type": "'.$capacities[3][0]->type.'",
                            "name": "'.$capacities[3][0]->name.'",
                            "description": "'.$capacities[3][0]->description.'",
                            "heal": '.$capacities[3][0]->heal.',
                            "dmg": '.$capacities[3][0]->dmg.',
                            "mana": '.$capacities[3][0]->mana.',
                            "gainMana": '.$capacities[3][0]->gainMana.',
                            "buff": '.$capacities[3][0]->buff.',
                            "combo": '.$capacities[3][0]->combo.'
                        }
                    ]
                }
            },
            "monster": {
                "pv": '.$monster->pv.',
                "pvMax": '.$monster->pvMax.',
                "mana" : '.$monster->mana.',
                "manaMax" : '.$monster->manaMax.',
                "attack": '.$monster->attack.'
            },
            "playerIdWhoPlayed": []
        }';

        // dd($monster,$capacities,$character,$characterName,$output);
        return $output;


    }
    
    public function get() {
        return '{
             "user": {
        "secret" : "test"
    },
    "players": {
        "1": {
            "name": "Henry",
            "pv": 100,
            "pvMax": 100,
            "mana": 30,
            "manaMax": 30,
            "id": 1,
            "hasPlayed": false,
            "capacities": [
                {
                    "type": "offense",
                    "name": "Poing",
                    "description": "",
                    "heal": 0,
                    "dmg": 1,
                    "mana": 1,
                    "gainMana": 0,
                    "buff": "",
                    "combo": 3
                }
            ]
        },
        "2": {
            "name": "Vaé",
            "pv": 100,
            "pvMax": 100,
            "mana": 30,
            "manaMax": 30,
            "id": 2,
            "hasPlayed": false,
            "capacities": [
                {
                    "type": "offense",
                    "name": "Poing",
                    "description": "",
                    "heal": 0,
                    "dmg": 150,
                    "mana": 1,
                    "gainMana": 0,
                    "buff": "",
                    "combo": 3
                }
            ]
        },
        "3": {
            "name": "Pierre",
            "pv": 100,
            "pvMax": 100,
            "mana": 30,
            "manaMax": 30,
            "id": 3,
            "hasPlayed": false,
            "capacities": [
                {
                    "type": "offense",
                    "name": "Poing",
                    "description": "",
                    "heal": 0,
                    "dmg": 1,
                    "mana": 1,
                    "gainMana": 0,
                    "buff": "",
                    "combo": 3
                }
            ]
        },
        "4": {
            "name": "Louis",
            "pv": 100,
            "pvMax": 100,
            "mana": 30,
            "manaMax": 30,
            "id": 4,
            "hasPlayed": false,
            "capacities": [
                {
                    "type": "offense",
                    "name": "Poing",
                    "description": "",
                    "heal": 0,
                    "dmg": 1,
                    "mana": 1,
                    "gainMana": 0,
                    "buff": "",
                    "combo": 3
                }
            ]
        }
    },
    "monster": {
        "pv": 800,
        "pvMax": 800,
        "mana" : 1000,
        "manaMax" : 1000,
        "attack": 1
    },
    "playerIdWhoPlayed": []

}';
    }

    public function getUser($id = null) {
        if($id === null){
            return DB::table('users')->get();
        }else{
            return DB::table('users')->where('id', $id)->get();
        }
    }
}
