<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buff extends Model
{
     protected $table = 'buff';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];
}
