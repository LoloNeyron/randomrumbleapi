<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharacterName extends Model
{
     protected $table = 'character_names';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];
}
