<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
   protected $table = 'state';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'user_id', 'game_id', 'state'
    ];
}
