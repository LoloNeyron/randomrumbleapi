<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class monster extends Model
{
    protected $table = 'monster';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'pv',
        'pvMax',
        'mana',
        'manaMax',
        'attack'
    ];
}
