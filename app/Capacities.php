<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capacities extends Model
{
    protected $table = 'capacities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'type',
        'name',
        'description',
        'dmg',
        'heal',
        'mana',
        'buff',
        'gainMana',
        'combo'
    ];
}
