<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $table = 'character';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_user',
        'id_name',
        'ids_capacities',
        'pv',
        'pvMax',
        'mana',
        'manaMax',
        'hasPlayed'
    ];
}
