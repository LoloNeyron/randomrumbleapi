@extends('layouts.appAdmin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card text-white bg-dark">
                <div class="card-header">
                    <h1 class="card-title">State</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-10">
                            <h5 class="card-title">Gestion des States</h5>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-success">Add</button>
                        </div>
                    </div>
                    <p class="card-text">
                        <table class="table table-hover table-dark">
                            <thead class="text-center">
                                <tr>
                                    <th>Id</th>
                                    <th>User Id</th>
                                    <th>State</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @foreach ($stateData as $data)
                                <tr>
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->user_id }}</td>
                                    <td>{{ $data->state }}</td>
                                    <td>{{ $data->created_at }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_edit_{{ $data->id }}">Edit</button>
                                        <button type="button" class="btn btn-danger">Delete</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach ($stateData as $data)
    <div class="modal" id="modal_edit_{{ $data->id }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $data->id }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="form-control">
                        <div class="group-control">
                            <label for="id{{ $data->id }}">Id</label>
                            <input id="id{{ $data->id }}" type="number" class="input-control" value="{{ $data->id }}">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
@endforeach
@endsection
