@extends('layouts.appAdmin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">Gestion des élements</h1>
            <hr>
        </div>
        <div class="col-4 mt-3">
            <a href="{{ Route('games') }}" class="btn btn-primary btn-block"><h4 class="card-title my-auto">Games</h4></a>
        </div>
        <div class="col-4 mt-3">
            <a href="{{ Route('states') }}" class="btn btn-primary btn-block"><h4 class="card-title my-auto">States</h4></a>
        </div>
        <div class="col-4 mt-3">
            <a href="{{ Route('games') }}" class="btn btn-primary btn-block"><h4 class="card-title my-auto">Users</h4></a>
        </div>
        <div class="col-4 mt-3">
            <a href="{{ Route('games') }}" class="btn btn-primary btn-block"><h4 class="card-title my-auto">Monster</h4></a>
        </div>
        <div class="col-4 mt-3">
            <a href="{{ Route('capacities') }}" class="btn btn-primary btn-block"><h4 class="card-title my-auto">Capacities</h4></a>
        </div>
        <div class="col-4 mt-3">
            <a href="{{ Route('games') }}" class="btn btn-primary btn-block"><h4 class="card-title my-auto">Buff</h4></a>
        </div>
        <div class="col-4 mt-3">
            <a href="{{ Route('games') }}" class="btn btn-primary btn-block"><h4 class="card-title my-auto">Character</h4></a>
        </div>
        <div class="col-4 mt-3">
            <a href="{{ Route('games') }}" class="btn btn-primary btn-block"><h4 class="card-title my-auto">Character Name</h4></a>
        </div>
    </div>
</div>
@endsection
