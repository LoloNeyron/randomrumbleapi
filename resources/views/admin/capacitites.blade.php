@extends('layouts.appAdmin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card text-white bg-dark">
                    <div class="card-header">
                        <h1 class="card-title">capacities</h1>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-10">
                                <h5 class="card-title">Gestion des Capacitées</h5>
                            </div>
                            <div class="col-2">
                                @if(Auth::check())
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_create">Add</button>
                                @else
                                    <button type="button" class="btn btn-success" onclick="alert('Please log in !')">Add</button>
                                @endif
                            </div>
                        </div>
                        <p class="card-text">
                        <table class="table table-hover table-dark">
                            <thead class="text-center">
                            <tr>
                                <th>Id</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Heal</th>
                                <th>Damages</th>
                                <th>Mana</th>
                                <th>Gain Mana</th>
                                <th>Buff</th>
                                <th>Combo</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody class="text-center">
                            @foreach ($capacitiesData as $data)
                                <tr>
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->type }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->description }}</td>
                                    <td>{{ $data->heal }}</td>
                                    <td>{{ $data->dmg }}</td>
                                    <td>{{ $data->mana }}</td>
                                    <td>{{ $data->gainMana }}</td>
                                    <td>{{ $data->buff }}</td>
                                    <td>{{ $data->combo }}</td>
                                    <td>{{ $data->created_at }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_edit_{{ $data->id }}">Edit</button>
                                        @if(Auth::check())
                                            <form style="display: inline-block" action="{{ Route('destroycapacitie') }}" method="post">
                                                @csrf
                                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                                <input type="hidden" name="api_token" value="{{ Auth::user()->api_token }}">
                                                <input type="hidden" name="capacities_id" value="{{ $data->id }}">
                                                <button type="submit" class="btn btn-danger" onclick="confirm('are you sur to destroy this item ?');">Delete</button>
                                            </form>
                                        @else
                                            <button type="button" class="btn btn-danger" onclick="Alert('Please Login !')">Delete</button>
                                        @endif
                                        @if(Auth::check())
                                            <form style="display: inline-block" action="{{ Route('duplicateCapacitie') }}" method="post">
                                                @csrf
                                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                                <input type="hidden" name="api_token" value="{{ Auth::user()->api_token }}">
                                                <input type="hidden" name="capacities_id" value="{{ $data->id }}">
                                                <button type="submit" class="btn btn-info">Duplicate</button>
                                            </form>
                                        @else
                                            <button type="button" class="btn btn-info" onclick="Alert('Please Login !')">Duplicate</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::check())
        @foreach ($capacitiesData as $data)
            <div class="modal fade" id="modal_edit_{{ $data->id }}">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ $data->id }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <form action="" class="modal-body form-control">
                            <div class="group-control">
                                <label for="id{{ $data->id }}">Id</label>
                                <input id="id{{ $data->id }}" type="number" class="input-control" value="{{ $data->id }}">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_type">Type</label>
                                <input id="nc_type" name="nc_type" type="text" class="form-control col-6" value="{{$data->type}}">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_name">Name</label>
                                <input id="nc_name" name="nc_name" type="text" class="form-control col-6" value="{{$data->name}}">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_desc">Description</label>
                                <textarea id="nc_desc" name="nc_desc" class="form-control col-6" value="{{$data->description}}"></textarea>
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_heal">Heal</label>
                                <input id="nc_heal" name="nc_heal" type="number" class="form-control col-6" value="{{$data->heal}}">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_dmg">Damage</label>
                                <input id="nc_dmg" name="nc_dmg" type="number" class="input-control col-6" value="{{$data->dmg}}">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_mana">Mana</label>
                                <input id="nc_mana" name="nc_mana" type="number" class="form-control col-6" value="{{$data->mana}}">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_gainMana">Gain Mana</label>
                                <input id="nc_gainMana" name="nc_gainMana" type="number" class="form-control col-6" value="{{$data->gainMana}}">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_buff">Buff</label>
                                <input id="nc_buff" name="nc_buff" type="number" class="form-control col-6" value="{{$data->buff}}">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_cmb">Combo</label>
                                <input id="nc_cmb" name="nc_cmb" type="number" class="form-control col-6" value="{{$data->combo}}">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary">Save changes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach

        <div class="modal fade" id="modal_create">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create a new capacity</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ Route('createCapacitie') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                            <input type="hidden" name="api_token" value="{{ Auth::user()->api_token }}">
                            <div class="group-control row">
                                <label class="col-6" for="nc_type">Type</label>
                                <input id="nc_type" name="nc_type" type="text" class="form-control col-6" value="">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_name">Name</label>
                                <input id="nc_name" name="nc_name" type="text" class="form-control col-6" value="">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_desc">Description</label>
                                <textarea id="nc_desc" name="nc_desc" class="form-control col-6" value=""></textarea>
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_heal">Heal</label>
                                <input id="nc_heal" name="nc_heal" type="number" class="form-control col-6" value="">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_dmg">Damage</label>
                                <input id="nc_dmg" name="nc_dmg" type="number" class="input-control col-6" value="">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_mana">Mana</label>
                                <input id="nc_mana" name="nc_mana" type="number" class="form-control col-6" value="">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_gainMana">Gain Mana</label>
                                <input id="nc_gainMana" name="nc_gainMana" type="number" class="form-control col-6" value="">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_buff">Buff</label>
                                <input id="nc_buff" name="nc_buff" type="number" class="form-control col-6" value="">
                            </div>
                            <br>
                            <div class="group-control row">
                                <label class="col-6" for="nc_cmb">Combo</label>
                                <input id="nc_cmb" name="nc_cmb" type="number" class="form-control col-6" value="">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection
