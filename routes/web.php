<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Admin Routes
Route::get('/admin', 'adminController@index')->name('admin');
Route::get('/admingames', 'adminController@gestionGames')->name('games');
Route::get('/adminstate', 'adminController@gestionStates')->name('states');
Route::get('/admincapacities', 'adminController@gestionCapacities')->name('capacities');


Route::get('/get', 'usersController@get')->name('getUser');
Route::get('/getinitialstate', 'usersController@GetInitialState')->name('getInitialState');

//Users Controller routes
Route::get('/getuser/{id?}', 'usersController@getUser')->name('getUser');

//games Controller routes
Route::get('/getgamebyid/{id?}', 'gamesController@getGameById')->name('getGameById');
Route::get('/getgamebyuserid/{id}', 'gamesController@getGameByUserId')->name('getGameByUserId');
Route::post('/creategame', 'gamesController@createGame')->name('createGame');

//State Controller Routes
Route::get('/getstatebyid/{id?}', 'stateController@getStateById')->name('getStateById');
Route::get('/getstatebyuserid/{id}', 'stateController@getStateByUserId')->name('getStateByUserId');
Route::get('/getstatebygameid/{id}', 'stateController@getStateByGameId')->name('getStateByGameId');
Route::post('/createstate', 'stateController@createState')->name('createState');

//CapacitiesController Routes
Route::post('/createcapacities', 'capacitiesController@createCapacitie')->name('createCapacitie');
Route::post('/destroycapacities', 'capacitiesController@destroyCapacities')->name('destroycapacitie');
Route::post('/duplicatecapacities', 'capacitiesController@duplicateCapacities')->name('duplicateCapacitie');
